TRAINEE TEST - Teste técnico para desenvolvimento de sistemas
==========================================
![Fourcicle](https://static.wixstatic.com/media/53ed29_939de150e76c449b9fe6b1eca52457d7~mv2.png/v1/fill/w_159,h_89,al_c,q_80,usm_0.66_1.00_0.01/fourcicle_uhd-01-02.webp)

Seja bem vindo e obrigado por realizar o teste técnico para o time de tecnologia da Fourcicle / ASBPM.

# Projeto
Criar uma aplicação web contemplando a criação de um CRUD de clientes. Esta aplicação poderá ser desenvolvida em (Java ou C# ou Node JS ou React ou PHP ou Python, etc) e disponibilizado em uma conta pessoal do GIT.

# Objetivo
Avaliar a criatividade, determinação e qualidade no desenvolvimento de soluções.

# Descrição
Criar um sistema web de **Gestão de clientes** com a seguinte especificação funcional:

- Cadastro de clientes de pessoa física ou jurídica, contemplando:
    - Criar um CRUD web para realizar a gestão de clientes; 
        - Cadastrar;
        - Editar;
        - Excluir;
        - Pesquisar (por ID, Nome);
    - Validar a obrigatoriedade dos dados;
    - Pesquisar no grid pelo (ID e Nome);
    - Realizar a edição do cliente;
    - Realizar a exclusão do cliente;
- Conexão com o banco de dados MYSQL como sistema de gerenciamento de dados;
- Disponibilizar o projeto criado em sua conta do GIT;

Estrutura de dados para criação do CRUD.

Campo  | Tipo | Regra de validação
------------- | ------------- | -------------
ID  | Incremental | ID sequencial da tabela.
Nome  | Texto(60) | O campo não pode ser vazio.
Salário  | Numérico(12,2) | O campo não ser zero.

# Critérios de avaliação
- O layout, linguagem de programação e componentes fica ao critério do candidato;
- Organização, estrutura e qualidade do código serão avaliadas com critério;
- O candidato deve enviar a URL do projeto no GIT e instruções para instalar e testar a solução;
- Quaisquer outras **funções adicionais** serão consideradas na avaliação;

# Prazo de entrega
O prazo de entrega será informado ao candidato pelo responsável da Fourcicle / ASBPM.


* Referências:
    * https://www.fourcicle.com.br

# Créditos
- Criado por: [Ely Gonçalves](https://www.fourcicle.com.br)
- Criado em: 20/03/2017 11:13
- Atualizado em: 27/01/2020 18:29